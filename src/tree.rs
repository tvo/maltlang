#![allow(unused)]

use std::collections::VecDeque;
use std::iter::FusedIterator;
use std::mem;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Tree<Item> {
    Leaf(Item),
    Branch((Item, Vec<Tree<Item>>)),
}

impl<Item> Default for Tree<Item> where Item: Default {
    fn default() -> Self {
        Tree::Leaf(Default::default())
    }
}

impl<It> Tree<It> {
    pub fn node(&self) -> &It{
        match self {
            Tree::Leaf(x) => x,
            Tree::Branch((x, _)) => x,
        }
    }

    pub fn iter(&self) -> TreeIter<'_, It> {
        TreeIter {
            children: std::slice::from_ref(self),
            parent: None,
        }
    }

    pub fn iter_direct_children(&self) -> TreeChildrenIter<'_, It> {
        TreeChildrenIter {
            children: match self {
                Tree::Leaf(..) => &[],
                Tree::Branch(b) => b.1.as_slice(),
            },
        }
    }

    pub fn iter_direct_children_mut(&mut self) -> TreeChildrenIterMut<'_, It> {
        TreeChildrenIterMut {
            children: match self {
                Tree::Leaf(..) => &mut [],
                Tree::Branch(b) => b.1.as_mut_slice(),
            },
        }
    }

    pub fn iter_deep_where<P>(&self, predicate: P) -> TreeIterDeepWhere<'_, It, P> where P : FnMut(&Tree<It>) -> bool {
        TreeIterDeepWhere {
            predicate,
            iter: self.iter()
        }
    }

    pub fn iter_bfs(&self) -> TreeIterBfs<'_, It> {
        TreeIterBfs {
            iter_list: [self].into(),
        }
    }

    pub fn iter_mut(&mut self) -> TreeIterMut<'_, It> {
        TreeIterMut {
            children: std::slice::from_mut(self),
            parent: None,
        }
    }
}

pub struct TreeIter<'a, It> {
    children: &'a [Tree<It>],
    parent: Option<Box<TreeIter<'a, It>>>,
}

impl<It> Default for TreeIter<'_, It> {
    fn default() -> Self {
        TreeIter {
            children: &[],
            parent: None,
        }
    }
}

impl<'a, It> Iterator for TreeIter<'a, It> {
    type Item = &'a Tree<It>;

    fn next(&mut self) -> Option<Self::Item> {
        let children = mem::take(&mut self.children);
        match children.split_first() {
            None => match self.parent.take() {
                Some(parent) => {
                    *self = *parent;
                    self.next()
                }
                None => None,
            },
            Some((first, rest)) => {
                self.children = rest;
                match first {
                    Tree::Branch((item, children)) => {
                        *self = TreeIter {
                            children: children.as_slice(),
                            parent: Some(Box::new(mem::take(self))),
                        };
                    },
                    Tree::Leaf(..) => {},
                };
                Some(first)
            }
        }
    }
}

pub struct TreeIterDeepWhere<'a, It, P> where P: FnMut(&Tree<It>) -> bool {
    predicate: P,
    iter: TreeIter<'a, It>,
}

impl<'a, It, P> Iterator for TreeIterDeepWhere<'a, It, P> where P: FnMut(&Tree<It>) -> bool {
    type Item = &'a Tree<It>;

    fn next(&mut self) -> Option<Self::Item> {
        let children = mem::take(&mut self.iter.children);
        match children.split_first() {
            None => match self.iter.parent.take() {
                Some(parent) => {
                    self.iter = *parent;
                    self.next()
                }
                None => None,
            },
            Some((first, rest)) => {
                self.iter.children = rest;
                match first {
                    _ => {},
                    Tree::Branch((item, children)) if (self.predicate)(first) => {
                        self.iter = TreeIter {
                            children: children.as_slice(),
                            parent: Some(Box::new(mem::take(&mut self.iter))),
                        };
                    }
                };
                Some(first)
            }
        }
    }
}


impl<It> FusedIterator for TreeIter<'_, It> {}

impl<'a, It> IntoIterator for &'a Tree<It> {
    type Item = &'a Tree<It>;
    type IntoIter = TreeIter<'a, It>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

pub struct TreeIntoIter<It> {
    children: VecDeque<Tree<It>>,
    parent: Option<Box<TreeIntoIter<It>>>,
}

impl<It> Default for TreeIntoIter<It> {
    fn default() -> Self {
        TreeIntoIter {
            children: Default::default(),
            parent: None,
        }
    }
}

impl<It> Iterator for TreeIntoIter<It> {
    type Item = It;

    fn next(&mut self) -> Option<Self::Item> {
        match self.children.pop_front() {
            None => match self.parent.take() {
                Some(parent) => {
                    // continue with the parent node
                    *self = *parent;
                    self.next()
                }
                None => None,
            },
            Some(Tree::Leaf(item)) => Some(item),
            Some(Tree::Branch((item, children))) => {
                // start iterating the child trees
                *self = TreeIntoIter {
                    children: children.into(),
                    parent: Some(Box::new(mem::take(self))),
                };
                Some(item)
            }
        }
    }
}

impl<It> FusedIterator for TreeIntoIter<It> {}

impl<It> IntoIterator for Tree<It> {
    type Item = It;
    type IntoIter = TreeIntoIter<It>;

    fn into_iter(self) -> Self::IntoIter {
        let mut children = VecDeque::with_capacity(1);
        children.push_back(self);
        TreeIntoIter {
            children,
            parent: None,
        }
    }
}

pub struct TreeIterMut<'a, It> {
    children: &'a mut [Tree<It>],
    parent: Option<Box<TreeIterMut<'a, It>>>,
}

impl<It> Default for TreeIterMut<'_, It> {
    fn default() -> Self {
        TreeIterMut {
            children: &mut [],
            parent: None,
        }
    }
}

impl<'a, It> Iterator for TreeIterMut<'a, It> {
    type Item = &'a mut It;

    fn next(&mut self) -> Option<Self::Item> {
        let children = mem::take(&mut self.children);
        match children.split_first_mut() {
            None => match self.parent.take() {
                Some(parent) => {
                    // continue with the parent node
                    *self = *parent;
                    self.next()
                }
                None => None,
            },
            Some((first, rest)) => {
                self.children = rest;
                match first {
                    Tree::Leaf(item) => Some(item),
                    Tree::Branch((item, children)) => {
                        *self = TreeIterMut {
                            children: children.as_mut_slice(),
                            parent: Some(Box::new(mem::take(self))),
                        };
                        Some(item)
                    }
                }
            }
        }
    }
}

impl<'a, It> IntoIterator for &'a mut Tree<It> {
    type Item = &'a mut It;

    type IntoIter = TreeIterMut<'a, It>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

pub struct TreeChildrenIter<'a, It> {
    children: &'a [Tree<It>],
}
impl<'a, It> Iterator for TreeChildrenIter<'a, It> {
    type Item = &'a Tree<It>;
    fn next(&mut self) -> Option<Self::Item> {
        match self.children.split_first() {
            Some((item, rest)) => {
                self.children = rest;
                Some(item)
            },
            None => None,
        }
    }
}

pub struct TreeChildrenIterMut<'a, It> {
    children: &'a mut [Tree<It>],
}
impl<'a, It> Iterator for TreeChildrenIterMut<'a, It> {
    type Item = &'a mut Tree<It>;
    fn next(&mut self) -> Option<Self::Item> {
        let mut children = std::mem::take(&mut self.children);
        match children.split_first_mut() {
            Some((item, rest)) => {
                self.children = rest;
                Some(item)
            },
            None => None,
        }
    }
}

pub struct TreeIterBfs<'a, It> {
    iter_list: VecDeque<&'a Tree<It>>,
}
impl<'a, It> Iterator for TreeIterBfs<'a, It> {
    type Item = &'a Tree<It>;
    fn next(&mut self) -> Option<Self::Item> {
        match self.iter_list.pop_front() {
            Some(item) => {
                match item {
                    Tree::Branch((_, to_add)) => {
                        for add in to_add {
                            self.iter_list.push_back(add);
                        }
                    },
                    _ => {},
                    //Tree::Leaf(add) => { self.iter_list.push_back(add); }
                }
                Some(item)
            },
            None => None,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_owned_iterator() {
        let tree = Tree::Branch((5, vec![
            Tree::Leaf(4),
            Tree::Branch((3, vec![Tree::Leaf(2)])),
            Tree::Branch((1, vec![Tree::Leaf(0), Tree::Leaf(-1)])),
        ]));

        let nums: Vec<i32> = tree.into_iter().collect();
        assert_eq!(nums, vec![5, 4, 3, 2, 1, 0, -1]);
    }

    #[test]
    fn test_owned_for_loop() {
        let tree = Tree::Leaf(42);

        for node in tree {
            let _: i32 = node;
        }
    }

    #[test]
    fn test_borrowing_iterator() {
        let tree = Tree::Branch((5, vec![
            Tree::Leaf(4),
            Tree::Branch((3, vec![Tree::Leaf(2)])),
            Tree::Branch((1, vec![Tree::Branch((0, vec![]))])),
        ]));

        let nums: Vec<i32> = tree.iter().map(|n| n.node()).copied().collect();
        assert_eq!(nums, vec![5, 4, 3, 2, 1, 0]);
    }

    #[test]
    fn test_borrowing_for_loop() {
        let tree = Tree::Leaf(42);
        for node in &tree {
            let _: Tree<i32> = node.clone();
        }
    }

    #[test]
    fn test_mut_iterator() {
        let mut tree = Tree::Branch((5, vec![
            Tree::Leaf(4),
            Tree::Branch((3, vec![Tree::Leaf(2)])),
            Tree::Branch((1, vec![Tree::Branch((0, vec![]))])),
        ]));

        for item in &mut tree {
            *item += 1;
        }

        assert_eq!(
            tree,
            Tree::Branch((6, vec![
                Tree::Leaf(5),
                Tree::Branch((4, vec![Tree::Leaf(3)])),
                Tree::Branch((2, vec![Tree::Branch((1, vec![]))])),
            ]))
        );
    }

    #[test]
    fn test_direct_children_iterator() {
        let mut tree = Tree::Branch((5, vec![
            Tree::Leaf(4),
            Tree::Branch((3, vec![Tree::Leaf(2)])),
            Tree::Branch((1, vec![Tree::Branch((0, vec![]))])),
        ]));
        let nums: Vec<i32> = tree.iter_direct_children().map(|n| n.node()).copied().collect();
        assert_eq!(nums, vec![4, 3, 1]);
    }

    #[test]
    fn test_direct_children_leaf_iterator() {
        let mut tree = Tree::Leaf(5);
        let nums: Vec<i32> = tree.iter_direct_children().map(|n| n.node()).copied().collect();
        assert_eq!(nums.len(), 0);
    }

    #[test]
    fn test_bfs() {
        let mut tree = Tree::Branch((5, vec![
            Tree::Leaf(4),
            Tree::Branch((3, vec![Tree::Leaf(1)])),
            Tree::Branch((2, vec![Tree::Branch((0, vec![]))])),
        ]));
        let nums: Vec<i32> = tree.iter_bfs().map(|n| n.node()).copied().collect();
        assert_eq!(nums, vec![5, 4, 3, 2, 1, 0]);
    }
}
