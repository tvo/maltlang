use crate::util::search_pattern;
use std::fmt::Debug;

#[derive(Debug)]
pub struct StateList<T, TId> {
    pub type_ids: Vec<TId>,
    pub data: Vec<T>,
}
impl<T, TId> StateList<T, TId> where T : Default {
    fn new() -> StateList<T, TId> {
        return StateList {
            type_ids: vec![],
            data: vec![],
        }
    }
    fn push(&mut self, id: TId, data: T) {
        self.type_ids.push(id);
        self.data.push(data);
    }
    fn drain(&mut self, index: usize, count: usize) {
        if count != 0 {
            self.type_ids.drain(index..(index + count));
            self.data.drain(index..(index + count));
        }
    }
    fn replace(&mut self, index: usize, count: usize, replacement_id: TId, replacement: &mut T) {
        self.drain(index+1, count-1);
        self.type_ids[index] = replacement_id;
        self.data[index] = std::mem::take(replacement);
    }
}

pub struct Rule<'f, T, TId> {
    pub id: TId,
    pub pattern: Vec<TId>,
    pub func: Box<dyn 'f + FnMut(&mut [T]) -> Option<T>>,
}

pub struct Parser<'f, T, TId> {
    pub rules: Vec<Rule<'f, T, TId>>,
}

impl<'f, T, TId> Parser<'f, T, TId> where T : Debug, T: Clone, TId : PartialEq, TId : Clone, T: Default {
    pub fn new() -> Parser<'f, T, TId> {
        return Parser {
            rules: vec![],
        }
    }

    pub fn parse<I>(&mut self, initial_state: I) -> StateList::<T, TId>
        where I: Iterator<Item = (TId, T)>
    {
        let mut state = StateList::<T, TId>::new();
        for (id, item) in initial_state {
            state.push(id, item);
        }
        let mut update_counter = 0;
        let mut prev_counter = -1;
        loop {
            for rule in self.rules.iter_mut() {
                let i = search_pattern(state.type_ids.as_slice(), rule.pattern.as_slice());
                if i != -1 {
                    let u = i as usize;
                    let mut replace = (rule.func)(&mut state.data.as_mut_slice()[u..(u + rule.pattern.len())]);
                    if replace.is_some() {
                        state.replace(u, rule.pattern.len(), rule.id.clone(), replace.as_mut().unwrap()); 
                    }
                    update_counter += 1;
                    break;
                }
            }
            if update_counter == prev_counter {
                break;
            }
            prev_counter = update_counter;
        }
        return state;
    }

    pub fn add_relabel_rule(&mut self, from: TId, to: TId) {
        self.rules.push(Rule::<T, TId>{id:to, pattern: vec![from], func: Box::new(move |_| {
            return None;
        })});
    }
}

