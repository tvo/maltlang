#[derive(Debug, Clone)]
pub enum OneOf<A, B> {
    A(A),
    B(B),
}

impl<A, B> OneOf<A, B> {
    fn get_a(&self) -> Option<&A> {
        match self { Self::A(a) => Some(a), _ => None }
    }

    fn get_b(&self) -> Option<&B> {
        match self { Self::B(b) => Some(b), _ => None }
    }

    fn get_a_mut(&mut self) -> Option<&mut A> {
        match self { Self::A(a) => Some(a), _ => None }
    }

    fn get_b_mut(&mut self) -> Option<&mut B> {
        match self { Self::B(b) => Some(b), _ => None }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_get_a() {
        let a = OneOf::<i32, bool>::A(42);
        assert_eq!(*a.get_a().unwrap(), 42);
    }
}
