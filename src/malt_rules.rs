use std::{cell::RefCell, rc::Rc};

use itertools::Itertools;

use crate::{ast::{Ast, GetSource, GlobalAstId, TokenType}, lexer::Lexer, parser::{Parser, Rule}, tree::Tree, util::starts_with};

pub struct MaltParser<'a> {
    parser: Parser<'a, Tree<Ast<'a>>, TokenType>,
    lexer: Lexer<'a, 'a, TokenType>,
}

impl<'a> MaltParser<'a> {
    pub fn new() -> MaltParser<'a> {
        let mut s = MaltParser{
            lexer: Lexer::new(),
            parser: Parser::new(),
        };
        s.add_malt_rules();
        s
    }
    fn add_malt_rules(&mut self) {
        let current_indent = Rc::new(RefCell::new(0));
        self.add_scope_end_term(current_indent.clone());
        self.add_scope_begin_term(current_indent.clone());

        self.add_lex_string(TokenType::FunctionKw, "func");
        self.add_lex_string(TokenType::ReturnKw, "return");
        //s.add_lex_string(TokenType::ClassKw, "class");
        self.add_lex_string(TokenType::StructKw, "struct");
        self.add_lex_string(TokenType::IfKw, "if");
        self.add_lex_string(TokenType::ElseKw, "else");
        self.add_lex_string(TokenType::BoolValue, "true");
        self.add_lex_string(TokenType::BoolValue, "false");
        self.add_lex_char(TokenType::Space, ' ');
        self.add_lex_char(TokenType::Tab, '\t');
        self.add_lex_char(TokenType::Dot, '.');
        self.add_lex_char(TokenType::Comma, ',');
        self.add_lex_char(TokenType::Colon, ':');
        self.add_lex_char(TokenType::ParenLeft, '(');
        self.add_lex_char(TokenType::ParenRight, ')');
        self.add_lex_char(TokenType::BracketLeft, '[');
        self.add_lex_char(TokenType::BracketRight, ']');
        self.add_lex_char(TokenType::NewLine, '\n');
        self.add_lex_regex(TokenType::Number, "^[0-9]+(\\.[0-9]+)?");
        self.add_lex_regex(TokenType::Label, "^[a-zA-Z_][a-zA-Z0-9_]*");
        self.add_lex_string(TokenType::BooleanCompareOp, "==");
        self.add_lex_string(TokenType::BooleanCompareOp, ">=");
        self.add_lex_string(TokenType::BooleanCompareOp, "<=");
        self.add_lex_string(TokenType::BooleanCompareOp, "!=");
        self.add_lex_char(TokenType::Equals, '=');
        self.add_eof_term();
        // ---- done adding term rules ----

        self.append_child(&[TokenType::Program, TokenType::Statement]);
        self.append_child(&[TokenType::Program, TokenType::Function]);
        self.merge(&[TokenType::NewLine, TokenType::NewLine], TokenType::NewLine);

        self.append_child(&[TokenType::ScopeBegin, TokenType::Statement]);
        self.merge(&[TokenType::ScopeBegin, TokenType::ScopeEnd], TokenType::Scope);

        self.merge(&[TokenType::BracketLeft, TokenType::CSValue, TokenType::BracketRight], TokenType::Array);

        self.combine(&[TokenType::Label, TokenType::Colon, TokenType::Label], TokenType::TypedVariable);
        self.combine(&[TokenType::TypedVariable, TokenType::Comma, TokenType::TypedVariable], TokenType::CSVTypedVariables);
        self.append_child(&[TokenType::TypedVariable, TokenType::Comma, TokenType::TypedVariable]);

        self.combine(&[TokenType::Value, TokenType::Comma, TokenType::Value], TokenType::CSValue);
        self.append_child(&[TokenType::CSValue, TokenType::Comma, TokenType::Value]);

        self.combine(&[TokenType::FunctionKw, TokenType::Label], TokenType::FunctionSignature);
        self.combine(&[
            TokenType::FunctionSignature,
            TokenType::ParenLeft,
            TokenType::ParenRight,
            TokenType::Scope,
        ], TokenType::Function);
        self.combine(&[
            TokenType::FunctionSignature,
            TokenType::ParenLeft,
            TokenType::TypedVariable,
            TokenType::ParenRight,
            TokenType::Scope,
        ], TokenType::Function);
        self.combine(&[
            TokenType::FunctionSignature,
            TokenType::ParenLeft,
            TokenType::CSVTypedVariables,
            TokenType::ParenRight,
            TokenType::Scope,
        ], TokenType::Function);


        self.combine(&[TokenType::IfKw, TokenType::Value, TokenType::Scope], TokenType::If);
        self.combine(&[TokenType::ElseKw, TokenType::Scope], TokenType::Else);
        self.append_child(&[TokenType::If, TokenType::Else]);

        self.combine(&[TokenType::Value, TokenType::BooleanCompareOp, TokenType::Value], TokenType::BooleanCompare);
        self.parser.add_relabel_rule(TokenType::BooleanCompare, TokenType::Value);

        self.combine(&[TokenType::ReturnKw, TokenType::Value], TokenType::Return);
        self.append_child(&[TokenType::Return, TokenType::NewLine]);
        self.combine(&[TokenType::Label, TokenType::Equals, TokenType::Value], TokenType::Assignment);
        self.combine(&[TokenType::TypedVariable, TokenType::Equals, TokenType::Value], TokenType::Assignment);

        // interpret as value
        self.combine(&[TokenType::Number], TokenType::Value);
        self.combine(&[TokenType::Array], TokenType::Value);

        self.combine(&[
            TokenType::Label,
            TokenType::ParenLeft,
            TokenType::ParenRight,
        ], TokenType::Invoke);
        self.combine(&[TokenType::Invoke], TokenType::Value);
        self.combine(&[
            TokenType::Label,
            TokenType::ParenLeft,
            TokenType::Value,
            TokenType::ParenRight,
        ], TokenType::Invoke);
        self.combine(&[TokenType::Invoke], TokenType::Value);
        self.combine(&[
            TokenType::Label,
            TokenType::ParenLeft,
            TokenType::CSValue,
            TokenType::ParenRight,
        ], TokenType::Invoke);
        self.combine(&[TokenType::Invoke], TokenType::Value);

        // interpret as statement
        self.parser.add_relabel_rule(TokenType::Assignment, TokenType::Statement);
        self.parser.add_relabel_rule(TokenType::Return, TokenType::Statement);
        self.parser.add_relabel_rule(TokenType::If, TokenType::Statement);
        self.combine(&[TokenType::Value, TokenType::NewLine], TokenType::Statement);

        self.combine(&[TokenType::Label], TokenType::Load);
        self.combine(&[TokenType::Load], TokenType::Value);

        // eat new lines
        self.append_child(&[TokenType::ScopeBegin, TokenType::NewLine]);
        self.append_child(&[TokenType::Scope, TokenType::NewLine]);

        self.append_child(&[TokenType::Program, TokenType::EOF]);
    }

    fn add_lex_char(&mut self, token: TokenType, pattern: char){
        self.lexer.rule_ids.push(token);
        self.lexer.rules.push(Box::new(move |raw_input: &'a str| {
            match raw_input.chars().next() {
                Some(c) if c == pattern => Some(raw_input.split_at(1).0),
                _ => None,
            }
        }));
    }

    fn add_lex_string(&mut self, token:TokenType, pattern: &'static str) {
        self.lexer.rule_ids.push(token);
        self.lexer.rules.push(Box::new(move |raw_input: &'a str| {
            if raw_input.len() == 0 {
                return None;
            }
            if starts_with(raw_input.chars(), pattern.chars()) {
                return Some(raw_input.split_at(pattern.len()).0);
            }
            return None;
        }));
    }

    fn add_lex_regex(&mut self, token: TokenType, pattern: &'static str) {
        debug_assert!(pattern.chars().next().unwrap() == '^');
        let r = regex::Regex::new(pattern).expect("Only valid regex please.");
        self.lexer.rule_ids.push(token);
        self.lexer.rules.push(Box::new(move |raw_input: &'a str|
            match r.captures_iter(raw_input).next() {
                Some(c) => Some(c.get(0).unwrap().as_str()),
                _ => None,
            }
        ));
    }

    fn combine(&mut self, from: &[TokenType], to: TokenType) {
        self.parser.rules.push(Rule::<Tree<Ast<'a>>, TokenType>{id:to, pattern: from.to_vec(), func: Box::new(move |args| {
            return Some(Tree::Branch((Ast::new(to, args.source()), args.to_vec())));
        })});
    }

    fn append_child(&mut self, pattern: &[TokenType]) {
        self.parser.rules.push(Rule::<Tree<Ast<'a>>, TokenType>{id:pattern[0], pattern:pattern.to_vec(), func: Box::new(move |args| {
            let source = args.source();
            let split = args.split_first_mut();
            match split {
                Some((Tree::Leaf(f), _)) => Some(Tree::Branch((f.clone(), args.iter().skip(1).cloned().collect_vec()))),
                Some((Tree::Branch((me, my_children)), other_children)) => {
                    for other_child in other_children {
                        my_children.push(std::mem::take(other_child));
                    }
                    me.source = source;
                    None
                },
                None => None,
            }
        })});
    }

    fn merge(&mut self, pattern: &[TokenType], to: TokenType) {
        debug_assert!(pattern.len() >= 2, "the merge should have at least two elements in it.");
        self.parser.rules.push(Rule::<Tree<Ast<'a>>, TokenType>{id:to, pattern:pattern.to_vec(), func: Box::new(move |args| {
            let source = args.source();
            let split = args.split_first_mut();
            if split.is_none() {
                return None;
            }
            let (me, siblings) = split.unwrap();
            let nephews = siblings.iter_mut().flat_map(|s| s.iter_direct_children_mut());
            match me {
                Tree::Leaf(me_leaf) => {
                    Some(Tree::Branch((me_leaf.clone(), nephews.map(|a| a.clone()).collect_vec())))
                },
                Tree::Branch((me_ast, children)) => {
                    me_ast.source = source;
                    for mut c in nephews {
                        children.push(std::mem::take(&mut c));
                    }
                    None
                },
            }
        })});
    }

    fn add_eof_term(&mut self) {
        let mut eof: Option<*const u8> = None;
        self.lexer.rule_ids.push(TokenType::EOF);
        self.lexer.rules.push(Box::new(move |raw_input: &'a str| {
            if raw_input.len() != 0 || eof.map(|e| e == raw_input.as_ptr()).unwrap_or(true) {
                return None;
            };
            eof = Some(raw_input.as_ptr());
            return Some(raw_input);
        }));
    }

    fn add_scope_end_term(&mut self, indent: Rc<RefCell<i32>>) {
        let tabs_pattern = regex::Regex::new("^\n\t*").expect("Only valid regex please.");
        let double_nl = regex::Regex::new("^\n\n").expect("Only valid regex please.");
        self.lexer.rule_ids.push(TokenType::ScopeEnd);
        self.lexer.rules.push(Box::new(move |raw_input: &'a str| {
            let i: i32 = indent.borrow().clone();
            if raw_input.len() == 0 {
                if i > 0 {
                    indent.replace(i - 1);
                    return Some(&raw_input[0..0]);
                }
                return None;
            }
            if double_nl.captures_iter(raw_input).next().is_some() {
                return None;
            }
            return match tabs_pattern.captures_iter(raw_input).next() {
                Some(c) => {
                    let tabs = c.get(0).unwrap().as_str().len() as i32 - 1;
                    if tabs < i {
                        indent.replace(i - 1);
                        if tabs == i - 1 {
                            return Some(c.get(0).unwrap().as_str())
                        }
                        return Some(&raw_input[0..0])
                    }
                    return None;
                },
                _ => None,
            };
        }));
    }

    fn add_scope_begin_term(&mut self, indent: Rc<RefCell<i32>>) {
        let tabs_pattern = regex::Regex::new("^\n\t+").expect("Only valid regex please.");
        self.lexer.rule_ids.push(TokenType::ScopeBegin);
        self.lexer.rules.push(Box::new(move |raw_input: &'a str| {
            if raw_input.len() == 0 {
                return None;
            };
            let i = indent.borrow().clone();
            return match tabs_pattern.captures_iter(raw_input).next() {
                Some(c) => {
                    if c.get(0).unwrap().len() as i32 == i + 2 {
                        indent.replace(i + 1);
                        Some(c.get(0).unwrap().as_str())
                    } else { None }
                },
                _ => None,
            };
        }));
    }

    pub fn parse(&mut self, file_data: &'a str) -> Option<Tree<Ast<'a>>> {
        let mut program_seed: Vec<(TokenType, Tree<Ast<'a>>)> = vec![];
        program_seed.push((TokenType::Program, Tree::Leaf(Ast::new(TokenType::Program, file_data))));
        let lex_iter = self.lexer.iter(file_data).map(|data| {
            (data.id.clone(), Tree::Leaf(Ast::new(data.id.clone(), data.view)))
        }).filter(|x| x.0 != TokenType::Space && x.0 != TokenType::Tab).collect_vec();
        let seed:Vec<(TokenType, Tree<Ast<'a>>)> = program_seed.iter().chain(lex_iter.iter()).cloned().collect_vec();
        
        let mut state = self.parser.parse(seed.into_iter());
            //dbg!(&self.parser.state); //print current ast state
            //dbg!(&self.parser.state); //print current ast state
        //if self.parser.state.len() > 1 {
            ////print all rules
            ////dbg!(&self.parser.rules.iter().map(|f| ((f.id, f.pattern.clone()))).collect_vec());
            //dbg!(&state); //print current ast state
            //return None;
        //}
        if state.data.is_empty() {
            return None;
        }
        for (i, ast) in state.data[0].iter_mut().enumerate() {
            ast.global_id = GlobalAstId(i);
        }
        return state.data.first().cloned();
    }
}

