use std::collections::{HashMap, VecDeque};
use itertools::Itertools;

use crate::{ast::*, program_data::*};

type Tree<'a> = crate::tree::Tree<Ast<'a>>;

impl<'a> ProgramData<'a>
{

    pub fn analyze(&mut self) {
        self.ast_id_dict();
        self.initialize_metadata();
        self.decorate_primitive_types();
        self.link_symbols_up_scope_tree();
        self.symbol_table();
    }

    fn invoke_args(&self, ast: &'a Tree<'a>) -> Option<(&'a str, Vec<TypeId>)> {
        let mut arg_vec = ast.iter_direct_children().skip(2).filter(|a| a.node().id != TokenType::Comma).collect_vec();
        arg_vec.pop();
        let label = ast.find_child(TokenType::Label).unwrap().source();
        let arg_types = arg_vec.iter().map(|a| self.evaluate_type(a)).collect_vec();
        if arg_types.iter().fold(0, |ac, t| ac + (t.map(|_| 0).unwrap_or(1))) != 0 {
            return None
        }
        return Some((label, arg_types.iter().map(|a| a.unwrap()).collect_vec()))
    }

    fn link_symbols_up_scope_tree(&mut self) {
        let mut label_ids = self.metadata_iter().filter(|m| [MetaDataPurpose::Variable, MetaDataPurpose::Function, MetaDataPurpose::Invoke].contains(&m.purpose))
            .map(|m| (m.ast_id, m.purpose)).collect_vec();
        label_ids.sort_by_key(|m| m.0);

        for (ast_id, purpose) in label_ids {
            match purpose {
                MetaDataPurpose::Variable => {
                    match self.get_symbol(self.get_ast(&ast_id).unwrap()) {
                        Some(decl) => {
                            let variable_ast = self.get_ast(&ast_id).unwrap();
                            let decl_scope_id = self.scope_of_ast(variable_ast).unwrap().id;
                            let var_meta = self.get_meta_mut(&ast_id).unwrap();
                            var_meta.scope_id = decl_scope_id;
                            var_meta.declaration = Some(decl);
                        },
                        None => {
                            let ast = self.get_ast(&ast_id).unwrap();
                            let scope_id = self.scope_of_ast(ast).unwrap().id;
                            let scope = self.get_scope_mut(&scope_id).unwrap();
                            scope.add_symbol(ast, ast_id);
                            self.get_meta_mut(&ast_id).unwrap().scope_id = scope_id;
                        },
                    }
                },
                MetaDataPurpose::Function => {
                    let function_ast = self.get_ast(&ast_id).unwrap();
                    let scope_id = self.scope_of_ast(function_ast).unwrap().id;
                    let scope = self.get_scope_mut(&scope_id).unwrap();
                    //scope.add_type(SymbolReference{name: function_ast.source()}, );
                    //scope.add_function(function_label_ast, function_label_id);
                    self.get_meta_mut(&ast_id).unwrap().scope_id = scope_id;
                },
                MetaDataPurpose::Invoke => {
                    let ast = self.get_ast(&ast_id).unwrap();
                    let invoke_label_ast = self.get_ast(ast.find_child(TokenType::Label).unwrap()).unwrap();
                    match self.get_symbol(invoke_label_ast) {
                        Some(found_fn_id) => {
                            let decl_scope_id = self.scope_of_ast(self.get_ast(&found_fn_id).unwrap()).unwrap().id;
                            let var_meta = self.get_meta_mut(&ast_id).unwrap();
                            var_meta.scope_id = decl_scope_id;
                            var_meta.declaration = Some(found_fn_id);
                        },
                        None => {
                            self.get_meta_mut(&ast_id).unwrap().purpose = MetaDataPurpose::InvokeExternal;
                            self.invoke_args(ast)
                                .map(|(name, args)| self.get_available_implicit(name, args.as_slice()))
                                .flatten()
                                .map(|fn_type_id| {
                                    let fn_malt_type = self.get_type(&fn_type_id).unwrap().clone();
                                    let fn_malt_non_primitive = fn_malt_type.get_non_primitive().unwrap();
                                    self.add_implicit_type(SymbolReference{name: invoke_label_ast.source()}, fn_malt_non_primitive.clone());
                                });
                        },
                    }
                },
                _ => {},
            }
        }
    }

    fn symbol_table(&mut self) -> HashMap<GlobalAstId, TypeId> {
        let mut table = HashMap::new();
        // get symbols
        for meta in self.metadata_iter().filter(|m| m.declaration.is_none() && m.purpose != MetaDataPurpose::Primitive) {
            let ast = self.get_ast(&meta.ast_id).unwrap();
            table.insert(&meta.ast_id, (ast, meta.purpose, None));
        }
        // loop trying to set a symbol until done.
        for _ in 0..table.len() {
            let mut to_insert = None;
            for (&k, &v) in table.iter().filter(|(_,v)| v.2.is_none()) {
                let eval_type = self.evaluate_type(v.0);
                if eval_type.is_none() {
                    continue;
                }
                to_insert = Some((k, (v.0, v.1, Some(eval_type.unwrap()))));
                break;
            }
            match to_insert {
                Some((k, v)) => { let _ = table.insert(k, v); },
                None => { break; },
            };
        }
        // unwrap into symbol table.
        let mut result = HashMap::new();
        result.reserve(table.len());
        for (&k, v) in table.iter().filter(|(_, (_, _, v))| v.is_some()) {
            result.insert(*k, v.2.unwrap());
        }
        result
    }

    fn ast_id_dict(&mut self) {
        for child in self.root_ast.iter() {
            self.add_ast(child);
        }
    }

    fn initialize_metadata(&mut self) {
        let mut scope_stack = VecDeque::new();
        let mut parent_scope_id = self.get_root_scope().id;
        scope_stack.push_back(parent_scope_id);
        for ast in self.root_ast.iter_bfs() {
            let get_label = || ast.find_child(TokenType::Label).unwrap();
            match ast.node().id {
                TokenType::Scope => { self.add_scope(&Scope::new(ast, Some(parent_scope_id))); },
                TokenType::ScopeBegin => {
                    parent_scope_id = self.scope_of_ast(ast).unwrap().id;
                    scope_stack.push_back(parent_scope_id);
                },
                TokenType::ScopeEnd => {
                    scope_stack.pop_back();
                    parent_scope_id = *scope_stack.back().unwrap();
                },
                TokenType::Number |
                TokenType::BoolValue => { self.add_metadata(&MetaData::new(ast, parent_scope_id, MetaDataPurpose::Primitive)); },
                TokenType::Assignment => { self.add_metadata(&MetaData::new(get_label(), parent_scope_id, MetaDataPurpose::Variable)); },
                TokenType::Load => { self.add_metadata(&MetaData::new(get_label(), parent_scope_id, MetaDataPurpose::Symbol)); },
                TokenType::FunctionSignature => { self.add_metadata(&MetaData::new(get_label(), parent_scope_id, MetaDataPurpose::Function)); },
                TokenType::Invoke => { self.add_metadata(&MetaData::new(ast, parent_scope_id, MetaDataPurpose::Invoke)); },
                _ => {},
            };
        }
    }

    fn decorate_primitive_types(&mut self) {
        for a in self.root_ast.iter().map(|a| a.node()) {
            let primitive_type = match a.id {
                TokenType::Number if a.source.parse::<i32>().is_ok() => Some(PrimitiveType::Int),
                TokenType::Number if a.source.parse::<f32>().is_ok() => Some(PrimitiveType::Float),
                TokenType::BoolValue if a.source.parse::<bool>().is_ok() => Some(PrimitiveType::Bool),
                _ => None,
            };
            match primitive_type {
                Some(x) => {
                    let primitive_metadata = self.get_meta_mut(&a.global_id).unwrap_or_else(|| panic!("Text trying to parse: \"{a}\". Metadata for primitives should have been initialzied."));
                    primitive_metadata.variable_type = Some(TypeId::Primitive(x));
                },
                None => {},
            };
        }
    }

    pub fn function_return_type(&self, ast: &'a Tree<'a>) -> Option<TypeId> {
        debug_assert!(&[TokenType::FunctionSignature, TokenType::Program].contains(&ast.node().id), "Function return type should only ever be called on a function ast.");
        let all_return_types = ast.iter_deep_where(|r| !&[TokenType::FunctionSignature].contains(&r.node().id))
            .filter(|r| r.node().id == TokenType::Return)
            .map(|r| self.evaluate_type(r))
            .collect_vec();
        match all_return_types.len() {
            0 => Some(TypeId::Primitive(PrimitiveType::Void)),
            1 => all_return_types[0].clone(),
            _ => {
                let all_eq = all_return_types.iter().all_equal_value();
                all_eq.expect("All returns should have the same type for a function.");
                all_return_types[0].clone()
            }
        }
    }

    pub fn evaluate_type(&self, ast: &'a Tree<'a>) -> Option<TypeId> {
        match ast.node().id {
            TokenType::Value => ast.iter_direct_children().next().map(|a| self.evaluate_type(a)).flatten(),
            TokenType::BooleanCompare => Some(TypeId::Primitive(PrimitiveType::Bool)),
            TokenType::Number | TokenType::BoolValue => self.get_meta(ast).map(|m| m.variable_type).flatten(),
            TokenType::Invoke => {
                let label_ast = ast.find_child(TokenType::Label).unwrap();
                let found_func_type = self.get_symbol_type(label_ast);
                let f_signature = found_func_type.map(|f| self.get_type(f))
                    .flatten()
                    .map(|f| f.get_function())
                    .flatten();
                f_signature.map(|f| f.return_type)
            },
            //TokenType::Add => as.(id),
            _ => None,
        }
    }
}

