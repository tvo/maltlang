#[derive(Clone, Copy)]
#[derive(Debug)]
pub struct LexResult<'a, TId> {
    pub view: &'a str,
    pub id: TId,
}

pub struct Lexer<'fstr, 'f, TId>  where TId : Clone{
    pub rules: Vec<Box<dyn 'f + FnMut(&'fstr str) -> Option<&'fstr str>>>,
    pub rule_ids: Vec<TId>,
}

impl<'fstr, 'f, TId> Lexer<'fstr, 'f, TId> where TId : Clone {
    pub fn new() -> Lexer<'fstr, 'f, TId> {
        return Lexer {
            rules: vec![],
            rule_ids: vec![],
        }
    }

    pub fn iter(&mut self, source_file: &'fstr str) -> LexerIter<'_, 'fstr, 'f, TId> {
        return LexerIter { lexer: self, source_file, position: 0 };
    }
}

pub struct LexerIter<'lexer, 'fstr, 'f, TId> where TId : Clone {
    pub lexer: &'lexer mut Lexer<'fstr, 'f, TId>,
    pub source_file: &'fstr str,
    pub position: usize,
}

impl<'lexer, 'fstr, 'f, TId> LexerIter<'lexer, 'fstr, 'f, TId> where TId : Clone,
{

    pub fn match_rules(&mut self) -> Option<LexResult<'fstr, TId>> {
        let (_, remainder) = self.source_file.split_at(self.position);
        for (id, rule) in self.lexer.rules.iter_mut().enumerate() {
            let m = (*rule)(remainder);
            match m {
                Some(view) => {
                    return Some(LexResult{ id: self.lexer.rule_ids[id].clone(), view});
                },
                None => {}
            }
        }
        return None;
    }
}

impl<'lexer, 'fstr, 'f, TId> Iterator for LexerIter<'lexer, 'fstr, 'f, TId>  where TId : Clone, 'f: 'fstr {
    type Item = LexResult<'fstr, TId>;

    fn next(&mut self) -> Option<Self::Item> {
        return match self.match_rules() {
            None => None,
            Some(found) => {
                self.position += found.view.len();
                Some(found)
            },
        };
    }
}
