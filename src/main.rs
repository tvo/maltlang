use std::fs;

use program_data::*;
use inkwell::context::Context;
use llvm_codegen::CodeGen;

mod one_of;
mod util;
mod tree;
mod lexer;
mod ast;
mod program_data;
mod analyzer;
mod parser;
mod malt_rules;
mod llvm_codegen;

fn main() {
    let file = fs::read_to_string("plop.malt").expect("Should have been able to read the file");
    let mut malt = malt_rules::MaltParser::new();
    let ast = malt.parse(&file).expect("Should have exactly one result from parsing.").clone();
    let mut program_data = ProgramData::new(&ast);

    for (name, f_type) in CodeGen::implicit_functions().as_ref().iter() {
        program_data.add_available_implicit(SymbolReference{name}, f_type.clone())
    }
    program_data.analyze();

    let llcontext = Context::create();
    let mut llvm = CodeGen::new(&llcontext, &program_data);

    llvm.link_implicit_functions();
    llvm.process(&ast);
    llvm.run();
}

