use std::{cmp::{Ord, Ordering}, collections::HashMap, hash::Hash, rc::Rc};
use itertools::Itertools;
use crate::{ast::*, util::vec_initial_none};

type Tree<'a> = crate::tree::Tree<Ast<'a>>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum TypeId {
    Primitive(PrimitiveType),
    NonPrimitive(usize),
}

//impl Default for TypeId {
    //fn default() -> Self {
        //TypeId::Primitive(PrimitiveType::Void)
    //}
//}

#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum PrimitiveType {
    Void,
    Float,
    Int,
    Long,
    Short,
    Byte,
    Bool,
    //String,
}

//#[derive(Debug, Clone, Copy, PartialEq, Eq)]
//pub enum AnnotationLifetime {
    ////Scope,
    ////Global,
    ////Named
//}
//#[derive(Debug, Clone, Copy, PartialEq, Eq)]
//pub enum AnnotationLocation {
    ////Local,
    ////Static,
//}
//#[derive(Debug, Clone, Copy, PartialEq, Eq)]
//pub enum AnnotationChangeability {
    ////Immutable,
    ////Mutable
//}

//#[derive(Debug, Clone, Copy, PartialEq, Eq)]
//pub struct VariableDecoration {
    //pub changeability: AnnotationChangeability,
    //pub location: AnnotationLocation,
    //pub lifetime: AnnotationLifetime,
//}

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[allow(dead_code)]
pub enum NonPrimitiveType<'a> {
    Struct(StructSignature<'a>),
    Function(FunctionSignature),
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum MaltType<'a> {
    Primitive(PrimitiveType),
    NonPrimitive(Rc<NonPrimitiveType<'a>>),
}

impl<'a> PartialEq<PrimitiveType> for MaltType<'a> {
    fn eq(&self, other: &PrimitiveType) -> bool {
        match self {
            MaltType::NonPrimitive(..) => false,
            MaltType::Primitive(p) => p.eq(other)
        }
    }
}

#[derive(PartialEq, Eq, Clone, Debug, Ord)]
pub struct FunctionSignature {
    pub params: Rc<[TypeId]>,
    pub return_type: TypeId,
}

impl<'a> MaltType<'a> {
    pub fn get_non_primitive(&self) -> Option<&NonPrimitiveType<'a>> {
        match self {
            MaltType::NonPrimitive(npt) => Some(npt),
            _ => None
        }
    }
    pub fn get_function(&self) -> Option<&FunctionSignature> {
        match self {
            MaltType::NonPrimitive(r) => {
                match r.as_ref() {
                    NonPrimitiveType::Function(f) => Some(f),
                    _ => None,
                }
            },
            _ => None
        }
    }
    //pub fn get_primitive(&self) -> Option<&PrimitiveType> {
        //match self {
            //MaltType::Primitive(p) => Some(p),
            //_ => None
        //}
    //}
}

impl FunctionSignature {
    pub fn new(params: Rc<[TypeId]>, return_type: TypeId) -> Self {
        Self { params, return_type }
    }
}

impl std::hash::Hash for FunctionSignature {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.return_type.hash(state);
        for param in self.params.iter() {
            param.hash(state);
        }
    }
}

impl PartialOrd for FunctionSignature {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let mut result = self.return_type.cmp(&other.return_type);
        result = result.then(self.params.len().cmp(&other.params.len()));
        Some(result.then_with(|| {
            let mut inner = Ordering::Equal;
            for item in self.params.iter().zip(other.params.iter()) {
                inner = inner.then(item.0.cmp(item.1));
            }
            inner
        }))
    }
}

#[derive(PartialEq, Eq, Clone, Debug, Ord)]
pub struct StructSignature<'a> {
    pub fields: Rc<[(TypeId, &'a str)]>,
}

impl<'a> std::hash::Hash for StructSignature<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        for param in self.fields.iter() {
            param.hash(state);
        }
    }
}

impl<'a> PartialOrd for StructSignature<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        let result = self.fields.len().cmp(&other.fields.len());
        Some(result.then_with(|| {
            let mut inner = Ordering::Equal;
            for item in self.fields.iter().zip(other.fields.iter()) {
                inner = inner.then(item.0.cmp(item.1));
            }
            inner
        }))
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TypeLookup<'a> {
    Name(&'a str),
    NameAndGenericTypes((&'a str, &'a[MaltType<'a>])),
}

#[derive(Debug, Clone)]
pub struct Scope<'a> {
    pub id: GlobalAstId,
    pub parent: Option<GlobalAstId>,
    symbols: HashMap<&'a str, GlobalAstId>,
    types: HashMap<TypeLookup<'a>, TypeId>,
    //generic_types: HashMap<GlobalAstId, NonPrimitiveType<'a>>,
}

impl <'a> Scope<'a> {
    pub fn new(ast_global_id: &impl GetGlobalAstId, parent: Option<GlobalAstId>) -> Scope<'a> {
        Scope {
            id: ast_global_id.get_global_ast_id(),
            parent,
            symbols: HashMap::new(),
            types: HashMap::new(),
            //generic_types: HashMap::new(),
        }
    }

    pub fn add_symbol(&mut self, source: &impl GetSource<'a>, label_id: GlobalAstId) {
        self.symbols.insert(source.source(), label_id);
    }

    pub fn get_symbol(&self, source: &'a str) -> Option<GlobalAstId> {
        self.symbols.get(source).cloned()
    }

    pub fn add_type(&mut self, label_ast_id: TypeLookup<'a>, to_add: TypeId) {
        self.types.insert(label_ast_id, to_add);
    }

    pub fn get_type(&self, source: &TypeLookup<'a>) -> Option<TypeId> {
        self.types.get(source).cloned()
    }
}

impl<'a> GetGlobalAstId for Scope<'a> {
    fn get_global_ast_id(&self) -> GlobalAstId {
        self.id
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum MetaDataPurpose {
    Symbol,
    Variable,
    Function,
    Invoke,
    InvokeExternal,
    Primitive,
}

#[allow(dead_code)]
#[derive(Clone, Debug)]
pub struct MetaData {
    pub ast_id: GlobalAstId,
    pub scope_id: GlobalAstId,
    pub declaration: Option<GlobalAstId>,
    pub variable_type: Option<TypeId>,
    //pub generics: Option<Vec<MaltType<'a>>>,
    pub purpose: MetaDataPurpose,
}

impl MetaData {
    pub fn new(ast_id: &impl GetGlobalAstId, scope_id: GlobalAstId, purpose: MetaDataPurpose) -> MetaData {
        return MetaData {
            ast_id: ast_id.get_global_ast_id(),
            scope_id,
            declaration: None,
            variable_type: None,
            purpose,
        };
    }
}

pub struct SymbolReference<'a> {
    pub name: &'a str,
    //pub generic_arguments: Vec<MaltType<'a>>,
}

pub struct ProgramData<'a> {
    pub root_ast: &'a Tree<'a>,
    ast_dict: Vec<Option<&'a Tree<'a>>>,
    metadata: HashMap<GlobalAstId, MetaData>,
    types: HashMap<TypeId, MaltType<'a>>,
    scope: Vec<Scope<'a>>,
    implicit_references: Vec<(SymbolReference<'a>, TypeId)>,
    available_implicits: Vec<(SymbolReference<'a>, TypeId)>,
}

impl<'a> ProgramData<'a> {
    pub fn new(ast: &'a Tree<'a>) -> ProgramData<'a> {
        let count = ast.iter().last().unwrap().node().global_id.0 + 1;
        ProgramData {
            root_ast: ast,
            ast_dict: vec_initial_none(count),
            metadata: Default::default(),
            types: Default::default(),
            scope: vec![Scope::new(ast, None)],
            implicit_references: Default::default(),
            available_implicits: Default::default(),
        }
    }

    pub fn add_ast(&mut self, ast: &'a Tree<'a>) {
        self.ast_dict[ast.get_global_ast_id().0] = Some(ast);
    }

    pub fn get_ast(&self, id: &impl GetGlobalAstId) -> Option<&'a Tree<'a>> {
        self.ast_dict[id.get_global_ast_id().0]
    }

    pub fn get_type(&self, id: &TypeId) -> Option<&MaltType<'a>> {
        match id {
            TypeId::NonPrimitive(..) => self.types.get(id),
            TypeId::Primitive(PrimitiveType::Void) => Some(&MaltType::Primitive(PrimitiveType::Void)),
            TypeId::Primitive(PrimitiveType::Bool) => Some(&MaltType::Primitive(PrimitiveType::Bool)),
            TypeId::Primitive(PrimitiveType::Byte) => Some(&MaltType::Primitive(PrimitiveType::Byte)),
            TypeId::Primitive(PrimitiveType::Short) => Some(&MaltType::Primitive(PrimitiveType::Short)),
            TypeId::Primitive(PrimitiveType::Int) => Some(&MaltType::Primitive(PrimitiveType::Int)),
            TypeId::Primitive(PrimitiveType::Long) => Some(&MaltType::Primitive(PrimitiveType::Long)),
            TypeId::Primitive(PrimitiveType::Float) => Some(&MaltType::Primitive(PrimitiveType::Float)),
        }
    }

    pub fn add_type(&mut self, t: NonPrimitiveType<'a>) -> TypeId {
        let id = TypeId::NonPrimitive(self.types.len());
        self.types.insert(id, MaltType::NonPrimitive(Rc::new(t)));
        id
    }

    pub fn add_metadata(&mut self, meta: &MetaData) {
        self.metadata.insert(meta.ast_id, meta.clone());
    }

    pub fn get_meta(&self, id: &impl GetGlobalAstId) -> Option<&MetaData> {
        self.metadata.get(&id.get_global_ast_id())
    }

    pub fn get_meta_mut(&mut self, id: &impl GetGlobalAstId) -> Option<&mut MetaData> {
        self.metadata.get_mut(&id.get_global_ast_id())
    }

    pub fn add_scope(&mut self, scope: &Scope<'a>) {
        debug_assert_ne!(scope.parent, None);
        self.scope.push(scope.clone());
    }

    pub fn get_scope_mut(&mut self, id: &GlobalAstId) -> Option<&mut Scope<'a>> {
        let index = self.scope.iter().find_position(move |s| s.id == *id).map(|p| p.0);
        match index {
            Some(i) => Some(&mut self.scope[i]),
            None => None,
        }
    }

    pub fn get_root_scope(&self) -> &Scope<'a> {
        &self.scope[0]
    }

    pub fn get_symbol(&self, reference_label: &impl GetSource<'a>) -> Option<GlobalAstId> {
        self.scope_search(reference_label, |a| a.get_symbol(reference_label.source()))
    }

    pub fn get_symbol_type(&self, reference_label: &impl GetSource<'a>) -> Option<&TypeId> {
        let scope_ast_opt = self.scope_search(reference_label, |a| a.get_symbol(reference_label.source()));
        match scope_ast_opt {
            None => None,
            Some(ast_id) => {
                self.get_meta(&ast_id).map(|v| v.variable_type.as_ref()).flatten()
            }
        }
    }

    fn scope_search<T: 'a>(&self, start_location: &impl GetSource<'a>, predicate: impl FnMut(&Scope<'a>) -> Option<T>) -> Option<T>
    {
        let first_scope_id = self.scope_of_ast(start_location).map(|s| s.id).unwrap_or(self.scope[0].id);
        let mut iter = self.scope_iter_up(first_scope_id);
        iter.find_map(predicate)
    }

    fn scope_iter_up(&self, start: GlobalAstId) -> ScopeIterateUp<'a, '_> {
        ScopeIterateUp { scope: &self.scope, start:Some(start.clone()) }
    }

    pub fn scope_of_ast(&self, src: &impl GetSource<'a>) -> Option<&Scope> {
        for s in self.scope.iter().rev() {
            let sptr = self.get_ast(s).unwrap().source().as_bytes().as_ptr_range();
            let aptr = src.source().as_bytes().as_ptr_range();
            if sptr.start <= aptr.start && aptr.end <= sptr.end {
                return Some(s);
            }
        }
        return None;
    }

    pub fn metadata_iter(&self) -> impl Iterator<Item=&MetaData> {
        self.metadata.iter().map(|x| x.1)
    }

    pub fn add_implicit_type(&mut self, reference: SymbolReference<'a>, typ: NonPrimitiveType<'a>) {
        let type_id = self.types.iter().find_map(|(k, v)| {
            match v {
                MaltType::NonPrimitive(p) if p.as_ref() == &typ => Some(k.clone()),
                _ => None,
            }
        }).unwrap_or_else(|| self.add_type(typ.clone()));
        self.implicit_references.push((reference, type_id));
    }

    pub fn iter_implicits(&self) -> impl Iterator<Item=(&'a str, &MaltType<'a>)> {
        self.implicit_references.iter().filter_map(|(SymbolReference { name: label }, tid)| self.get_type(tid).map(|t| (*label, t)))
    }

    pub fn get_available_implicit(&self, name: &'a str, arg_types: &[TypeId]) -> Option<TypeId> {
        self.available_implicits.iter()
            .filter(|(i_n, i_f)| name == i_n.name && 
                self.get_type(i_f).map(|np| np.get_function())
                    .flatten()
                    .map(|f| f.params.as_ref().eq(arg_types))
                    .unwrap_or(false)
            ).map(|(_, i_f)| i_f)
            .next()
            .cloned()
    }

    pub fn add_available_implicit(&mut self, reference: SymbolReference<'a>, func: FunctionSignature) {
        let type_id = self.types.iter().find_map(|(k, v)| { 
            match v.get_function() == Some(&func) {
                true => Some(k.clone()),
                false => None,
            }
        }).unwrap_or_else(|| self.add_type(NonPrimitiveType::Function(func)));
        self.available_implicits.push((reference, type_id));
    }
}

pub struct ScopeIterateUp<'a, 'svec> {
    pub scope: &'svec Vec<Scope<'a>>,
    pub start: Option<GlobalAstId>,
}

impl<'a, 'svec> Iterator for ScopeIterateUp<'a, 'svec> {
    type Item = &'svec Scope<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        match self.start {
            None => None,
            Some(start) => self.scope.iter().find(|s| s.id == start).map(|x| { self.start = x.parent; x })
        }
    }
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_add_get_imp_type() {
        let ast: Tree<Ast> = Tree::Leaf(Ast::new(TokenType::None, "noneya business"));
        let mut pd = ProgramData::new(&ast);
        let f_sig = FunctionSignature::new(Rc::new([]), TypeId::Primitive(PrimitiveType::Void));
        pd.add_available_implicit(SymbolReference{name: "foobar"}, f_sig);
        let available = pd.get_available_implicit("foobar", &[]);
        assert_ne!(available, None);
    }

    #[test]
    pub fn test_get_primitive_type() {
        let ast: Tree<Ast> = Tree::Leaf(Ast::new(TokenType::None, "primitive type's"));
        let pd = ProgramData::new(&ast);
        assert_eq!(pd.get_type(&TypeId::Primitive(PrimitiveType::Void)), Some(&MaltType::Primitive(PrimitiveType::Void)));
        assert_eq!(pd.get_type(&TypeId::Primitive(PrimitiveType::Int)), Some(&MaltType::Primitive(PrimitiveType::Int)));
    }
}
