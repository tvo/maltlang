use std::{cmp::Ord, fmt, hash::Hash};
use crate::{tree::Tree, util::join_str};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TokenType {
    None = 0,
    Space,
    Dot,
    Tab,
    Comma,
    Colon,
    ParenLeft,
    ParenRight,
    BracketLeft,
    BracketRight,
    Equals,
    BooleanCompareOp,
    BooleanCompare,
    StructKw,
    Number,
    BoolValue,
    StringLiteral,
    ReturnKw,
    FunctionKw,
    FunctionSignature,
    IfKw,
    ElseKw,
    NewLine,
    Label,
    EOF,
    ScopeBegin,
    ScopeEnd,
    //Switch,
    //For,
    //While,

    Program,
    Load,
    Assignment,
    Expression,
    Add,
    Subtract,
    Multiply,
    Value,
    //Number,
    CSValue,
    Type,
    TypedVariable,
    CSVTypedVariables,
    //StringLiteral,
    Array,
    Invoke,
    Return,
    Function,
    If,
    Else,
    Scope,
    Statement,
    WhiteSpace,
    //Switch,
    //For,
    //While,
    //Scope,
    ImplicitIncludeFunctionDeclaration
}

impl Default for TokenType {
    fn default() -> Self {
        TokenType::None
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord, Default)]
pub struct GlobalAstId(pub usize);

pub trait GetGlobalAstId {
    fn get_global_ast_id(&self) -> GlobalAstId;
}
impl GetGlobalAstId for GlobalAstId {
    fn get_global_ast_id(&self) -> GlobalAstId {
        *self
    }
}

pub trait GetSource<'a> {
    fn source(&self) -> &'a str;
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct Ast<'a> {
    pub global_id: GlobalAstId,
    pub id: TokenType,
    pub source: &'a str,
}

impl<'a> GetGlobalAstId for Ast<'a> {
    fn get_global_ast_id(&self) -> GlobalAstId {
        self.global_id
    }
}

impl<'a> GetSource<'a> for Ast<'a> {
    fn source(&self) -> &'a str {
        self.source
    }
}

impl<'a> fmt::Display for Ast<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.source)
    }
}

impl<'a> Ast<'a> {
    pub fn new(id: TokenType, source: &'a str) -> Ast<'a> {
        Ast{id, global_id: GlobalAstId(0), source}
    }
}

impl<'a> Tree<Ast<'a>> {
    pub fn find_child<'c, 'me: 'c>(&'me self, id: TokenType) -> Option<&'c Tree<Ast<'a>>> {
        return self.iter().skip(1).find(|a| a.node().id == id);
    }
    pub fn source(&self) -> &'a str {
        match self {
            Tree::Leaf(x) => x.source,
            Tree::Branch(x) => x.0.source,
        }
    }
    pub fn set_source(&mut self, src: &'a str) {
        match self {
            Tree::Leaf(x) => x.source = src,
            Tree::Branch(x) => x.0.source = src,
        }
    }
}

impl<'a, T: GetSource<'a>> GetSource<'a> for [T]
{
    fn source(&self) -> &'a str {
        match self.len() {
            0 => panic!("Unable to create a str ref from an empty array"),
            1 => self[0].source(),
            _ => join_str(self[0].source(), self.last().unwrap().source()),
        }
    }
}

impl<'a> GetGlobalAstId for Tree<Ast<'a>> {
    fn get_global_ast_id(&self) -> GlobalAstId {
        self.node().global_id
    }
}
impl<'a> GetSource<'a> for Tree<Ast<'a>> {
    fn source(&self) -> &'a str {
        self.source()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_ast_source() {
        let src = "abcd";
        let mut ast: Ast = Default::default();
        ast.source = src;
        assert_eq!(ast.source(), src);
    }

    #[test]
    fn test_ast_array_one_source() {
        let ast: Ast = Ast::new(Default::default(), "abcd");
        assert_eq!([ast].source(), "abcd");
    }

    #[test]
    fn test_ast_array_two_source() {
        let src = "0123456789";
        let (left, right) = src.split_at(5);
        let l: Ast = Ast::new(Default::default(), left);
        let r: Ast = Ast::new(Default::default(), right);
        assert_eq!([l, r].source(), "0123456789");
    }

    #[test]
    fn test_ast_array_three_source() {
        let src = "0123456789";
        let (left, rest) = src.split_at(2);
        let (mid, right) = rest.split_at(2);
        let l: Ast = Ast::new(Default::default(), left);
        let m: Ast = Ast::new(Default::default(), mid);
        let r: Ast = Ast::new(Default::default(), right);
        assert_eq!([l, m, r].source(), "0123456789");
    }
}
