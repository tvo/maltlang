use core::panic;
use std::collections::HashMap;
use std::rc::Rc;
use inkwell::types::{BasicMetadataTypeEnum, BasicType, PointerType};
use inkwell::values::{BasicMetadataValueEnum, BasicValue, BasicValueEnum, InstructionValue, PointerValue};
use inkwell::builder::Builder;
use inkwell::context::Context;
use inkwell::execution_engine::{ExecutionEngine, JitFunction};
use inkwell::{OptimizationLevel, AddressSpace};
use inkwell::module::Module;
use itertools::Itertools;

use crate::ast::*;
use crate::program_data::*;
use crate::tree::Tree;

struct StateInfo<'ctx> {
    variable_address: Option<PointerValue<'ctx>>,
    pointer_type: Option<PointerType<'ctx>>,
}

pub struct CodeGen<'ctx, 'a, 'm> {
    context: &'ctx Context,
    module: Module<'ctx>,
    builder: Builder<'ctx>,
    program_data: &'m ProgramData<'a>,
    llstate: HashMap<GlobalAstId, StateInfo<'ctx>>,
}

impl<'ctx, 'a, 'm> CodeGen<'ctx, 'a, 'm> {
    pub fn new(context: &'ctx Context, program_data: &'m ProgramData<'a>) -> CodeGen<'ctx, 'a, 'm> {
        return CodeGen {
            context,
            module: context.create_module("malt"),
            builder: context.create_builder(),
            program_data,
            llstate: HashMap::new(),
        };
    }

   pub fn implicit_functions() -> Rc<[(&'static str, FunctionSignature)]> {
        Rc::new([
            ("print", FunctionSignature::new(Rc::new([TypeId::Primitive(PrimitiveType::Int)]), TypeId::Primitive(PrimitiveType::Void))),
        ])
    }

    pub fn link_implicit_functions(&mut self) {
        for imp in self.program_data.iter_implicits().filter_map(|i| i.1.get_function().map(|f| (i.0, f.params.as_ref()))) {
            match imp {
                ("print", [TypeId::Primitive(PrimitiveType::Int)]) => {
                    let print_type = self.context.void_type().fn_type(&[BasicMetadataTypeEnum::IntType::<'ctx>(self.context.i32_type())], false);
                    self.module.add_function("print", print_type, Some(inkwell::module::Linkage::AvailableExternally));
                }
                (label, t) => {
                    dbg!(&label, t);
                    dbg!(&t);
                    panic!("Unable to link type with label: {}", label);
                }
            }
        }
    }

    pub fn process(&mut self, ast: &Tree<Ast<'a>>) -> Option<CodeGenResult<'ctx>>{
        return match ast.node().id {
            TokenType::None => None,
            TokenType::Assignment => {
                let label_ast = ast.find_child(TokenType::Label).unwrap();
                let value_ast = ast.find_child(TokenType::Value).unwrap();
                let meta = self.program_data.get_meta(label_ast).unwrap();
                let declaration = match meta {
                    MetaData { declaration: Some(d_id), ..} => self.program_data.get_ast(d_id).unwrap().node(),
                    _ => label_ast.node(),
                };
                let variable = match meta.declaration {
                    None => self.builder.build_alloca(self.context.i32_type(), declaration.source),
                    //Some(ast_decl) => self.builder.(self.context.f64_type(), ""),
                    _ => todo!(),
                }.ok().unwrap();
                let ptr_type = self.context.ptr_type(AddressSpace::default());
                self.llstate.insert(label_ast.node().global_id, StateInfo{ variable_address: Some(variable), pointer_type: Some(ptr_type)});
                let instruction = match self.process(value_ast) {
                    Some(CodeGenResult::Value(v)) => self.builder.build_store(variable, v).unwrap(),
                    _ => panic!("Can only assign a value to a variable."),
                };
                return Some(CodeGenResult::Instruction(instruction));
            },
            TokenType::Number | TokenType::BoolValue => {
                let meta_type_id = self.program_data.get_meta(ast).unwrap().variable_type.expect("Constant primitive should have a variable pre parsed.");
                let meta_type = match meta_type_id { TypeId::Primitive(p) => p, _ => panic!("Non primitive type stored as a primitive.") };
                let src = ast.source();
                let value = match meta_type {
                    PrimitiveType::Int => BasicValueEnum::IntValue(self.context.i32_type().const_int(src.parse::<i32>().unwrap() as u64, false)),
                    PrimitiveType::Bool => BasicValueEnum::IntValue(self.context.bool_type().const_int(src.parse::<bool>().unwrap() as u64, false)),
                    PrimitiveType::Float => BasicValueEnum::FloatValue(self.context.f32_type().const_float(src.parse::<f32>().unwrap() as f64)),
                    _ => panic!("unable to parse primitive."),
                };
                Some(CodeGenResult::Value(value))
            },
            TokenType::Label => todo!("label not load?"),
            TokenType::Load => {
                let declaration = self.program_data.get_meta(ast).map(|x| x.declaration).flatten().expect("Metadata not generated.");
                let state = self.llstate.get(&declaration).expect("Address not found.");
                let ptr_type = state.pointer_type.unwrap();
                let address = state.variable_address.unwrap();
                let load_instr = self.builder.build_load(ptr_type, address, self.program_data.get_ast(&declaration).unwrap().source()).unwrap();
                return Some(CodeGenResult::Value(load_instr));
            },
            TokenType::Value => self.process(ast.iter().skip(1).next().expect("Value should have at least one child. It's an NTerm.")),
            TokenType::Statement => self.process(ast.iter().skip(1).next().expect("Statements neeed to have a child.")),
            TokenType::Return => {
                let return_value_ast = ast.find_child(TokenType::Value);
                let return_value_opt = match return_value_ast {
                    Some(x) => self.process(x),
                    _ => None,
                };
                return match return_value_opt {
                    Some(CodeGenResult::Value(value)) => {
                        let instruction = self.builder.build_return(Some(&value)).unwrap();
                        Some(CodeGenResult::Return(instruction))
                    },
                    None => {
                        let z = self.context.i32_type().const_zero().as_basic_value_enum();
                        return Some(CodeGenResult::Return(self.builder.build_return(Some(&z)).ok().expect("Default return value")))
                    },
                    _ => panic!("value was not found from expression."),
                };
            },
            TokenType::BooleanCompare => {
                let op_ast = ast.find_child(TokenType::BooleanCompareOp).expect("if block must have a boolean compare like ==.");
                let op = match op_ast.source() {
                    "==" => inkwell::IntPredicate::EQ,
                    "!=" => inkwell::IntPredicate::NE,
                    ">=" => inkwell::IntPredicate::SGE,
                    "<=" => inkwell::IntPredicate::SLE,
                    ">" => inkwell::IntPredicate::SGT,
                    "<" => inkwell::IntPredicate::SLT,
                    _ => panic!("unable to parse the boolean comparison inside if.")
                };
                let ast_children = ast.iter_direct_children().collect_vec();
                let children_values = ast_children.iter().filter(|x| x.node().id == TokenType::Value).collect_vec();
                let l_ast = children_values.first().expect("if left must be a value.");
                let r_ast = children_values.last().expect("if right must be a value.");
                let lhs_code = self.process(l_ast).expect("must parse lhs down to a value.");
                let rhs_code = self.process(r_ast).expect("must parse rhs down to a value.");
                let lhs = match lhs_code {
                    CodeGenResult::Value(x) => x,
                    _ => panic!(),
                };
                let rhs = match rhs_code {
                    CodeGenResult::Value(x) => x,
                    _ => panic!(),
                };
                let lcast = self.builder.build_ptr_to_int(lhs.into_pointer_value(), self.context.i32_type(), "").unwrap();
                let comp_opt = self.builder.build_int_compare(op, lcast, rhs.into_int_value(), "");
                let comp = comp_opt.unwrap();
                return Some(CodeGenResult::Value(comp.as_basic_value_enum()));
            },
            TokenType::If => {
                // TODO: this only handles int / boolean comparison.
                let bool_ast = ast.find_child(TokenType::BooleanCompare).expect("'if' should have a boolean value.");
                let op_value = match self.process(bool_ast) {
                    Some(CodeGenResult::Value(v)) => Some(v),
                    _ => None,
                }.expect("value ast should parse down to a single value for if statements.");

                let parent_fn = self.builder.get_insert_block().unwrap().get_parent().unwrap();
                let then_ast = ast.find_child(TokenType::Scope).unwrap();
                let else_ast = ast.find_child(TokenType::Else);
                let then_block = self.context.append_basic_block(parent_fn, "then");
                let merge_block = self.context.append_basic_block(parent_fn, "if_merge");
                let else_block = match else_ast {
                    Some(_) => self.context.append_basic_block(parent_fn, "else"),
                    None => merge_block,
                };

                self.builder.build_conditional_branch(op_value.into_int_value(), then_block, else_block).unwrap();

                // if block
                self.builder.position_at_end(then_block);
                self.process(then_ast);
                self.builder.build_unconditional_branch(merge_block).unwrap();

                // else block
                if else_ast.is_some() {
                    self.builder.position_at_end(else_block);
                    self.process(else_ast.unwrap());
                    self.builder.build_unconditional_branch(merge_block).unwrap();
                }
                self.builder.position_at_end(merge_block);
                // maybe do phi to have if/else return one value.
                //self.builder.build_conditional_branch(comparison, then_block, else_block)
                None
            },
            TokenType::Invoke => {
                let label_ast = ast.find_child(TokenType::Label).unwrap();
                let fn_name = label_ast.source();
                let mut arg_vec = ast.iter_direct_children().skip(2).filter(|a| a.node().id != TokenType::Comma).collect_vec();
                arg_vec.pop();
                let arg_types = arg_vec.iter().map(|a| self.process(a)).collect_vec();
                let arguments = arg_types.iter().map(|a| match a {
                    Some(CodeGenResult::Value(v)) => Self::basic_value_to_meta_value(*v),
                    _ => panic!("Trying to pass non value to invoke a function? tisk")
                }).collect_vec();

                //let meta = self.program_data.get_meta(label_ast).unwrap();

                let func = self.module.get_function(fn_name).expect(&("Function not found in module: ".to_string() + fn_name));
                let return_value = self.builder.build_call(func, arguments.as_slice(), fn_name).expect("function should be called.");
                let value = return_value.try_as_basic_value().left().unwrap_or(BasicValueEnum::IntValue(self.context.i32_type().const_int(0, false)));
                //let value = BasicValueEnum::IntValue(self.context.i32_type().const_int(42 as u64, false));
                return Some(CodeGenResult::Value(value))
            },
            TokenType::Function => {
                let fn_signature = ast.find_child(TokenType::FunctionSignature).expect("Looking for fn signature.");
                let fn_name = fn_signature.find_child(TokenType::Label).expect("Looking for fn name.").source();
                let meta = &self.program_data.get_meta(ast).unwrap();
                let malt_return_type_id = meta.variable_type.expect("Function should have variable type defined.");
                let malt_return_type = self.program_data.get_type(&malt_return_type_id).unwrap();
                let void_fn = self.context.void_type().fn_type(&[], false);
                let fn_type = match malt_return_type {
                    MaltType::Primitive(PrimitiveType::Void) => void_fn,
                    _ => self.convert_malt_type(malt_return_type).unwrap().fn_type(&[], false),
                };
                let is_void = match malt_return_type { 
                    MaltType::Primitive(PrimitiveType::Void) => true,
                    _ => false,
                };
                let fn_val = self.module.add_function(fn_name, fn_type, None);
                let previous_block_save_state = self.builder.get_insert_block().unwrap();
                let block = self.context.append_basic_block(fn_val, "entry");
                self.builder.position_at_end(block);

                let function_body_iter = ast.find_child(TokenType::Scope)
                    .expect("Scope expected for function.")
                    .iter_direct_children();
                let result = self.function_body(function_body_iter, is_void);
                self.builder.position_at_end(previous_block_save_state);

                return Some(result);
            },
            TokenType::Scope => {
                match ast {
                    Tree::Branch((_, children)) => {
                        for c in children.iter() {
                            self.process(c);
                        }
                    },
                    _ => {},
                }
                return Some(CodeGenResult::Other)
            },
            TokenType::Program => {
                let i32_type = self.context.i32_type();
                let fn_type = i32_type.fn_type(&[], false);
                let fn_val = self.module.add_function("main", fn_type, None);
                let block = self.context.append_basic_block(fn_val, "entry");
                self.builder.position_at_end(block);
                let fn_return_type = self.program_data.get_type(&self.program_data.function_return_type(ast).unwrap()).unwrap();
                let is_void = match fn_return_type {
                    MaltType::Primitive(PrimitiveType::Void) => true,
                    _ => false
                };
                return Some(self.function_body(ast.iter_direct_children(), is_void))
            },
            _ => None,
        }
    }

    fn function_body<'i, I>(&mut self, ast_iter: I, is_void: bool) -> CodeGenResult<'ctx>
        where I: Iterator<Item = &'i Tree<Ast<'a>>>, 'a : 'i
    {
        let mut return_statement: Option<CodeGenResult<'ctx>> = None;
        for c in ast_iter {
            return_statement = match self.process(c) {
                Some(CodeGenResult::Return(r)) => Some(CodeGenResult::Return(r)),
                _ => return_statement,
            }
        }
        return match return_statement {
            Some(CodeGenResult::Return(_)) => return_statement.unwrap(),
            _ => {
                if is_void {
                    let z = self.context.i32_type().const_zero().as_basic_value_enum();
                    CodeGenResult::Return(self.builder.build_return(Some(&z)).ok().expect("Default return value"))
                } else {
                    panic!("No return was found for this function");
                }
            },
        }
    }

    pub fn add_in_prints(&self, engine: &ExecutionEngine<'ctx>) {
        let extr_print_fn_int = self.module.get_function("print").unwrap();
        engine.add_global_mapping(&extr_print_fn_int, print as usize);
    }

    pub fn run(&self) {
        let execution_engine = self.module.create_jit_execution_engine(OptimizationLevel::None).unwrap();
        self.add_in_prints(&execution_engine);
        self.module.print_to_stderr();
        unsafe {
            type MaltMain = unsafe extern "C" fn() -> i32;
            let malt: JitFunction<MaltMain> = execution_engine.get_function("main").unwrap();
            let result_code = malt.call();
            println!("Result: {}", result_code);
        }
    }

    fn convert_malt_type(&self, malt_type: &MaltType) -> Option<inkwell::types::BasicTypeEnum<'ctx>> {
        match malt_type {
            MaltType::Primitive(PrimitiveType::Int) => Some(inkwell::types::BasicTypeEnum::IntType(self.context.i32_type())),
            MaltType::Primitive(PrimitiveType::Bool) => Some(inkwell::types::BasicTypeEnum::IntType(self.context.i32_type())),
            MaltType::Primitive(PrimitiveType::Float) => Some(inkwell::types::BasicTypeEnum::FloatType(self.context.f32_type())),
            MaltType::Primitive(PrimitiveType::Void) => None,
            _ => todo!("More type conversion code please!"),
        }
    }

    fn basic_value_to_meta_value(value: BasicValueEnum<'ctx>) -> BasicMetadataValueEnum<'ctx> {
        match value {
            BasicValueEnum::ArrayValue(v) => BasicMetadataValueEnum::ArrayValue(v),
            BasicValueEnum::IntValue(v) => BasicMetadataValueEnum::IntValue(v),
            BasicValueEnum::FloatValue(v) => BasicMetadataValueEnum::FloatValue(v),
            BasicValueEnum::PointerValue(v) => BasicMetadataValueEnum::PointerValue(v),
            BasicValueEnum::StructValue(v) => BasicMetadataValueEnum::StructValue(v),
            BasicValueEnum::VectorValue(v) => BasicMetadataValueEnum::VectorValue(v),
        }
    }

}

#[no_mangle]
pub extern "C" fn print(x: i32) {
    println!("{}", x);
    //std::io::stdout().flush();
}
#[used]
static EXTERNAL_FNS: [extern "C" fn(i32); 1] = [print];

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum CodeGenResult<'ctx> {
    Other,
    Instruction(InstructionValue<'ctx>),
    Return(InstructionValue<'ctx>),
    Value(BasicValueEnum<'ctx>),
}
