FROM archlinux as builder
#ENV CC=gcc-11 CXX=g++-11
ENV CC=clang CXX=clang++
WORKDIR /src
#RUN yes | pacman -Syu --noconfirm
RUN rm -rf /etc/pacman.d/gnupg/* && pacman-key --init && pacman-key --populate archlinux
RUN yes | pacman -Sy --noconfirm wget curl zip tar archlinux-keyring llvm rust clang
#RUN yes | pacman -Sy --noconfirm 

COPY . .

RUN llvm-config --version
#RUN echo "$(which $CC) $(which $CXX)"

#RUN cmake build . -DCMAKE_C_COMPILER="$(which gcc)" -DCMAKE_CXX_COMPILER="$(which g++)" -G Ninja -DCMAKE_TOOLCHAIN_FILE=/opt/vcpkg/scripts/buildsystems/vcpkg.cmake
#RUN cmake build -G Ninja -DCMAKE_C_COMPILER="$(which $CC)" -DCMAKE_CXX_COMPILER="$(which $CXX)" -DCMAKE_TOOLCHAIN_FILE=/vcpkg/scripts/buildsystems/vcpkg.cmake .
#RUN ninja clean && ninja && ninja test
#RUN mkdir -p /artifacts && cp malt /artifacts

RUN cargo build -r

FROM archlinux
COPY --from=builder /src/target/release/malt malt
#COPY --from=builder /src/libLLVM-15.so libLLVM-15.so
ENTRYPOINT ./malt


